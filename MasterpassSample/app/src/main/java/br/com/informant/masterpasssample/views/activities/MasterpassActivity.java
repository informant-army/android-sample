package br.com.informant.masterpasssample.views.activities;

import android.support.v4.app.Fragment;

import br.com.informant.masterpasssample.R;
import br.com.informant.masterpasssample.views.fragments.MasterpassFragment;

/**
 * Created by Lucas Cordeiro - @lucasmacielc on GitHub, on 08/09/2017
 */

public class MasterpassActivity extends AbstractActivity {

    @Override
    public Fragment setFragmentActivity() {
        return new MasterpassFragment();
    }

    @Override
    public int setLayoutActivity() {
        return R.layout.activity_default;
    }
}
