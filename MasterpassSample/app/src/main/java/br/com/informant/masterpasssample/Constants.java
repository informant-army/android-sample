package br.com.informant.masterpasssample;

/**
 * Created by Lucas Cordeiro - @lucasmacielc on GitHub, on 08/09/17
 */
public class Constants {

    // Put your callback url here
    public static final String MASTERPASS_CALLBACK_URL = "http://sample.masterpass.informant.com.br/redirect.html";
    public static final String MASTERPASS_BUTTONIMAGE_URL = "https://static.masterpass.com/dyn/img/btn/global/mp_chk_btn_147x034px.svg";
    // Put your masterpass chekoutID here
    public static final String MASTERPASS_CHECKOUTID = "95f1ea5373f64018a0f2577d119289cc";

}
